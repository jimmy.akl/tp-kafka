package if4030.kafka;

/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements. See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.KTable;
import org.apache.kafka.streams.kstream.Produced;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.BufferedReader;
import java.io.FileReader;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Arrays;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.CountDownLatch;
import java.util.regex.Pattern;

public final class BookProcessing{
    public static final String INPUT_TOPIC = "lines-streams";
    public static final String MID_TOPIC = "words-streams";
    public static final String OUTPUT_TOPIC = "tagged-words-stream";
    public static final String END_TOPIC = "command-topic";
    
    private static Map<String, Long> wordCountVER = new HashMap<>();
    private static Map<String, Long> wordCountNOM = new HashMap<>();

    static Properties getStreamsConfig(final String[] args) throws IOException {
        final Properties props = new Properties();
        if (args != null && args.length > 0) {
            try (final FileInputStream fis = new FileInputStream(args[0])) {
                props.load(fis);
            }
            if (args.length > 1) {
                System.out.println("Warning: Some command line arguments were ignored. This demo only accepts an optional configuration file.");
            }
        }
        props.putIfAbsent(StreamsConfig.APPLICATION_ID_CONFIG, "autor-analyser");
        props.putIfAbsent(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        props.putIfAbsent(StreamsConfig.STATESTORE_CACHE_MAX_BYTES_CONFIG, 0);
        props.putIfAbsent(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass().getName());
        props.putIfAbsent(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.String().getClass().getName());

        // setting offset reset to earliest so that we can re-run the demo code with the same pre-loaded data
        // Note: To re-run the demo, you need to use the offset reset tool:
        // https://cwiki.apache.org/confluence/display/KAFKA/Kafka+Streams+Application+Reset+Tool
        props.putIfAbsent(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
        return props;
    }

    static void createWordStream(final StreamsBuilder builder) {
        final KStream<String, String> source = builder.stream(INPUT_TOPIC);

        final KStream<String, String> words = source
            .flatMapValues(value -> Arrays.asList(value.toLowerCase(Locale.getDefault()).split("\\s+")))
            .filter((key, value) -> Pattern.matches("([a-z]|-)+", value));

        words.peek(( key, value ) -> System.out.println( "key: " + key + " - value: " + value ));

        words.to(MID_TOPIC, Produced.with(Serdes.String(), Serdes.String()));
    }

    static void createTaggedWordStream(final StreamsBuilder builder) throws IOException {
        final KStream<Object, Object> source = builder.stream(MID_TOPIC);

        Dictionary<String,String> categorie = new Hashtable<String,String>();
        Dictionary<String,String> lemme = new Hashtable<String,String>();
        BufferedReader TSVReader = new BufferedReader(new FileReader("./Lexique383.tsv"));
        
        String line = null;
        while((line = TSVReader.readLine()) != null) {
            String[] words = line.split("\t");
            categorie.put(words[0], words[3]);
            lemme.put(words[0], words[2]);
        }

        TSVReader.close();

        final KStream<String, String> taggedWords = source
            .map((key,value) -> new KeyValue<>(categorie.get(value),lemme.get(value)));
        
        taggedWords.peek(( key, value ) -> System.out.println( "key: " + key + " - value: " + value ));    
        taggedWords.to(OUTPUT_TOPIC, Produced.with(Serdes.String(), Serdes.String()));
    };

    static void createWordCountStream(final StreamsBuilder builder) {
        final KStream<String, String> words = builder.stream(OUTPUT_TOPIC);
        final KStream<String, String> stop = builder.stream(END_TOPIC);

        words.peek((key, value) -> countWords(key, value));

        stop.filter((key, value) -> value.equals("END")).peek((key, value) -> printTop20Words());
    }

    static void countWords(String cat, String lem) {
        if (cat == null) return;
        if (cat.equals("VER")) {    
            if (wordCountVER.get(lem) == null) {
                wordCountVER.put(lem, 1L);
            } else {
                wordCountVER.replace(lem, wordCountVER.get(lem) + 1L);
            }
        } else if (cat.equals("NOM")) {
            if (wordCountNOM.get(lem) == null) {
                wordCountNOM.put(lem, 1L);
            } else {
                wordCountNOM.replace(lem, wordCountNOM.get(lem) + 1L);
            }
        }
    }

    public static void printTop20Words() {
        System.out.println("Top 20 VER:");
        wordCountVER.entrySet()
            .stream()
            .sorted(Map.Entry.<String, Long>comparingByValue().reversed())
            .limit(20)
            .forEach(entry -> System.out.println(entry.getKey() + ": " + entry.getValue()));
    
        System.out.println("\nTop 20 NOM:");
        wordCountNOM.entrySet()
            .stream()
            .sorted(Map.Entry.<String, Long>comparingByValue().reversed())
            .limit(20)
            .forEach(entry -> System.out.println(entry.getKey() + ": " + entry.getValue()));
    }
    

    public static void main(final String[] args) throws IOException {
        final Properties props = getStreamsConfig(args);

        final StreamsBuilder builder = new StreamsBuilder();
        createWordStream(builder);
        createTaggedWordStream(builder);
        createWordCountStream(builder);
        final KafkaStreams streams = new KafkaStreams(builder.build(), props);
        final CountDownLatch latch = new CountDownLatch(1);

        // attach shutdown handler to catch control-c
        Runtime.getRuntime().addShutdownHook(new Thread("autor-comparation-shutdown-hook") {
            @Override
            public void run() {
                streams.close();
                latch.countDown();
            }
        });

        try {
            streams.start();
            latch.await();
        } catch (final Throwable e) {
            System.exit(1);
        }
        System.exit(0);
    }
}
